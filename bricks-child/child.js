let lastKnownScrollPosition = 0;
let ticking = false;

const navbar = document.querySelector("#navbar");
const isTransparentNavbar = document
  .querySelector("body")
  .classList.contains("transparent-nav");

const updateNavbar = (position) => {
  navbar.classList[position > 0 ? "add" : "remove"]("scrolled");
};

document.addEventListener("scroll", () => {
  lastKnownScrollPosition = window.scrollY;

  if (!ticking) {
    window.requestAnimationFrame(() => {
      updateNavbar(lastKnownScrollPosition);
      ticking = false;
    });

    ticking = true;
  }
});

updateNavbar(window.scrollY);

const callback = (entries, observer) => {
  entries.forEach((entry) => {
    const target = entry.target;
    switch (target.getAttribute("observer")) {
      case "fade-in-children": {
        if (entry.isIntersecting) {
          if (target.classList.contains("children-hidden")) {
            target.classList.remove("children-hidden");
            [...target.children].forEach((child, i) => {
              setTimeout(() => {
                child.classList.remove("opacity-0");
              }, i * 250);
            });
          }

          observer.unobserve(target);
        } else {
          target.classList.add("children-hidden");
          [...target.children].forEach((child) => {
            child.classList.add("opacity-0");
            child.classList.add("transition-opacity");
          });
        }
        break;
      }
    }
  });
};

const observer = new IntersectionObserver(callback, {
  root: null,
  rootMargin: "0px",
  threshold: 0.1,
});

const targets = document.querySelectorAll("[observer]");
targets.forEach((target) => {
  observer.observe(target);
});
