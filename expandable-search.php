<?php 
if ( ! defined( 'ABSPATH' ) ) exit; // Exit hvis filen bliver tilgået direkte

class Prefix_Element_Test extends \Bricks\Element {
  // Element properties
  public $category     = 'general';                     // Bruger element kategorien "general"
  public $name         = 'expandable-search';           // Prefix for elementet
  public $icon         = 'ti-search';                   // Themify icon for elementet
  public $css_selector = '.expandable-search-wrapper';  // Standard CSS selector for elementet
  public $scripts      = [''];                          // Script(s) for når elementet er synligt

  // Returnerner lokaliseret element navn (oversat med l10n)
  public function get_label() {
    return esc_html__( 'Expandable Search', 'bricks' );
  }
 
  // Sætter element controller
  public function set_controls() {
    $this->controls['placeholderText'] = [
			'tab'     => 'content',
			'label'   => esc_html__( 'Placeholder Text', 'bricks' ),
			'type'    => 'text',
			'inline'  => true,
			'default' => "Search ..",
		];
  }

  // Render funktionen for elementet
  public function render() {
    $root_classes[] = 'prefix-test-wrapper';

		$settings       	= $this->settings;

    // Placeholder tekst - tjekker om der er indtastet en placeholder tekst, bruges dynamisk data, ellers bruges standard teksten "Search .."
    $placeholder_text = ! empty( $settings['placeholderText'] ) ? $this->render_dynamic_data( $settings['placeholderText'] ) : 'Search ..';

    // Tilføjer 'class' attribut til elementet
    $this->set_attribute( '_root', 'class', $root_classes );

    // Render elementets HTML
    // '_root' er et krav siden Bricks 1.4 (indeholder elementets root attributter, så som klasser og ID)
    echo "<div {$this->render_attributes( '_root' )}>";
		echo '<div class="search-container">
						<form action="/" method="get">
							<input class="search expandright" id="searchright" type="search" name="s" placeholder="'.htmlspecialchars($placeholder_text).'">
							<label class="button searchbutton bricks-background-secondary" for="searchright"><i class="fa-solid fa-search"></i></label>
						</form>
					</div>';
    echo '</div>';
  }
}