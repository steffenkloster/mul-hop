const { chromium, firefox, webkit, devices } = require("playwright");
const fs = require("fs");
const compareImages = require("resemblejs/compareImages");
const path = require("path");
const chalk = require("chalk");

// List of URLs to visit
const urls = [
  "https://hop.steffen.codes/",
  "https://hop.steffen.codes/ydelser/",
  "https://hop.steffen.codes/hjemmeside-eller-webshop/",
  "https://hop.steffen.codes/seo-og-adwords/",
  "https://hop.steffen.codes/hosting-og-e-mail/",
  "https://hop.steffen.codes/omkring-telanco/",
  "https://hop.steffen.codes/kontakt-os/",
  "https://hop.steffen.codes/google-analytics-alternativ-til-en-cookie-fri-hjemmeside/", // Blog post
  "https://hop.steffen.codes/404/", // 404 side
  "https://hop.steffen.codes/branche/bygge-og-anlaegsselskaber/" // SEO-lead page
];

const headless = true;
const skipScreenshots = true;
const skipCompare = false;

// Bricks Breakpoints:
//  - Desktop (base)
//  - Tablet portrait (<= 991px)
//  - Mobile landscape (<= 767px)
//  - Mobile portrait (<= 478px)

// Resolution statistics source: https://gs.statcounter.com/screen-resolution-stats/all/denmark/#monthly-202305-202405
const viewports = {
  pc1920: { width: 1920, height: 1080 }, // (Most common desktop resolution in Denmark:     15.54% of all users, May 2024)
  pc1280: { width: 1280, height: 720 }, // (Smallest common desktop resolution in Denmark:  5.87% of all users, May 2024)
  tablet: { width: 766, height: 1024 }, // (Most common tablet resolution in Denmark:       1.44% of all users, May 2024)
  mobile: { width: 390, height: 844 } // (Most common mobile resolution in Denmark:         5.08% of all users, May 2024)
};

const browsers = {
  chromium: chromium,
  firefox: firefox,
  webkit: webkit
};

// Source: https://stackoverflow.com/a/34509653/17904373
function ensureDirectoryExistence(filePath) {
  var dirname = path.dirname(filePath);
  if (fs.existsSync(dirname)) {
    return true;
  }
  ensureDirectoryExistence(dirname);
  fs.mkdirSync(dirname);
}

function replaceURL(url) {
  return url.replace(/https?:\/\//, "").replace(/\W/g, "_");
}

async function getDiff(
  sourceImage,
  secondImage,
  browserName,
  browserResolution,
  pageName
) {
  const options = {
    output: {
      errorColor: {
        red: 255,
        green: 0,
        blue: 255
      },
      errorType: "flat",
      transparency: 1,
      largeImageThreshold: 0,
      useCrossOrigin: false,
      outputDiff: true
    },
    scaleToSameSize: true,
    ignore: "antialiasing"
  };

  const data = await compareImages(
    await fs.promises.readFile(sourceImage),
    await fs.promises.readFile(secondImage),
    options
  );

  const twoDecimals = (num) => (Math.round(num * 100) / 100).toFixed(2);

  const differencePercentage = data.rawMisMatchPercentage;

  const savePath = `differences/${browserName}/${browserResolution}/${pageName}.png`;
  ensureDirectoryExistence(savePath);
  await fs.promises.writeFile(savePath, data.getBuffer());

  return twoDecimals(differencePercentage);
}

(async () => {
  if (skipScreenshots) {
    console.log(
      chalk.bgYellow.black("[!] Skipping generating screenshots of pages")
    );
  }

  for (const [browserName, browserType] of Object.entries(browsers)) {
    if (skipScreenshots) {
      continue;
    }

    const browser = await browserType.launch({ headless: headless });

    for (const [deviceName, viewport] of Object.entries(viewports)) {
      const context = await browser.newContext({
        viewport: { width: viewport.width, height: viewport.height }
      });

      for (const url of urls) {
        console.log(
          `Navigating to ${url} on ${browserName} with ${deviceName}`
        );

        const page = await context.newPage();
        await page.goto(url);

        await page.evaluate(async () => {
          const delay = (ms) =>
            new Promise((resolve) => setTimeout(resolve, ms));
          for (let i = 0; i < document.body.scrollHeight; i += 100) {
            window.scrollTo(0, i);
            await delay(100);
          }
        });

        await new Promise((resolve) => setTimeout(resolve, 1000));

        await page.evaluate(async () => {
          window.scrollTo(0, 0);
        });

        await new Promise((resolve) => setTimeout(resolve, 1000));

        const fileName = `screenshot-${browserName}-${deviceName}-${replaceURL(
          url
        )}.png`;
        await page.screenshot({
          path: `screenshots/${fileName}`,
          fullPage: true
        });

        console.log(
          `Screenshot taken for ${url} on ${browserName} with ${deviceName} and saved as ${fileName}`
        );
        await page.close();
      }

      await context.close();
    }

    await browser.close();
  }

  if (skipCompare) {
    console.log(
      chalk.bgYellow.black("[!] Skipping comparing screenshots of pages")
    );
  }

  const differenceReport = {
    urls: {},
    notices: {}
  };

  const noticeColors = {
    NONE: "green",
    NOTICE: "blue",
    WARNING: "yellow",
    EXTRA_WARNING: "red"
  };

  for (const [browserName, _] of Object.entries(browsers)) {
    if (skipCompare) {
      continue;
    }

    if (browserName == "chromium") {
      continue;
    }

    for (const [deviceName, _] of Object.entries(viewports)) {
      for (const url of urls) {
        console.log(
          `Generating differences for ${url} on ${browserName} with ${deviceName}`
        );

        const sourceImage = `screenshots/screenshot-chromium-${deviceName}-${replaceURL(
          url
        )}.png`;
        const secondImage = `screenshots/screenshot-${browserName}-${deviceName}-${replaceURL(
          url
        )}.png`;

        const difference = await getDiff(
          sourceImage,
          secondImage,
          browserName,
          deviceName,
          replaceURL(url)
        );

        const differenceNum = parseFloat(difference);
        let noticeLevel = "NONE";
        if (differenceNum >= 2)
          noticeLevel =
            differenceNum < 5
              ? "NOTICE"
              : differenceNum < 10
              ? "WARNING"
              : "EXTRA_WARNING";

        console.log(
          chalk(
            `Pixel difference is: ${chalk[noticeColors[noticeLevel]].bold(
              difference + "%"
            )}`
          )
        );

        differenceReport.urls[url] = differenceReport.urls[url] || {};

        differenceReport.urls[url][browserName] =
          differenceReport.urls[url][browserName] || {};

        differenceReport.urls[url][browserName][deviceName] = difference;

        if (noticeLevel !== "NONE") {
          differenceReport.notices[noticeLevel] =
            differenceReport.notices[noticeLevel] || [];

          differenceReport.notices[noticeLevel].push({
            url,
            browserName,
            deviceName,
            difference
          });
        }
      }
    }
  }

  if (!skipCompare) {
    const jsonContent = JSON.stringify(differenceReport, null, 2);
    await fs.promises.writeFile("differenceReport.json", jsonContent);
    console.log("differenceReport saved to differenceReport.json");
  }
})();
