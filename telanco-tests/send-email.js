import nodemailer from "nodemailer";
import dotenv from "dotenv";
import MailosaurClient from "mailosaur";

dotenv.config();

const transporter = nodemailer.createTransport({
  host: process.env.SMTP_HOST,
  port: process.env.SMTP_PORT,
  secure: true, // true for 465, false for other ports
  auth: {
    user: process.env.SMTP_USER,
    pass: process.env.SMTP_PASS
  }
});

export const sendTestEmail = async (to, subject, text) => {
  const info = await transporter.sendMail({
    from: `"Test Sender" <${process.env.SMTP_USER}>`,
    to,
    subject,
    text
  });

  return info;
};

const mailosaur = new MailosaurClient(process.env.MAILOSAUR_API_KEY);

export const verifyEmailReceived = async (serverId, sentTo) => {
  const result = await mailosaur.messages.search(serverId, {
    sentTo
  });

  if (result.items.length > 0) {
    const email = result.items[0];
    return email;
  }

  throw new Error("No email received");
};
