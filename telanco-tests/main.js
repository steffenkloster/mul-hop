import { exec } from "child_process";

const runTests = (command) => {
  return new Promise((resolve, reject) => {
    const process = exec(command, (error, stdout, stderr) => {
      if (error) {
        reject(error);
      } else {
        resolve(stdout);
      }
    });

    process.stdout.on("data", (data) => {
      console.log(data.toString());
    });

    process.stderr.on("data", (data) => {
      console.error(data.toString());
    });
  });
};

const runAllTests = async () => {
  try {
    console.log("Running Jest tests...");
    await runTests("npx jest");

    console.log("Running Playwright tests...");
    await runTests("npx playwright test --config=playwright.config.cjs");
  } catch (error) {
    console.error("Error running tests:", error);
    process.exit(1);
  }
};

runAllTests();
