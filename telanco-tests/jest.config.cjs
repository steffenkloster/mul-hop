module.exports = {
  transform: {
    "^.+\\.js$": "babel-jest"
  },
  testMatch: ["**/tests/jest/**/*.test.js"],
  testTimeout: 30000 // 30 seconds
};
