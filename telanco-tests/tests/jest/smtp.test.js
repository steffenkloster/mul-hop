import { sendTestEmail, verifyEmailReceived } from "../../send-email";
import MailosaurClient from "mailosaur";
import randomstring from "randomstring";

describe("SMTP Server Testing", () => {
  test("should send and receive an email successfully", async () => {
    const mailosaur = new MailosaurClient(process.env.MAILOSAUR_API_KEY);
    const serverId = process.env.MAILOSAUR_SERVER_ID;
    const testEmail = mailosaur.servers.generateEmailAddress(serverId);

    console.log(`Sending e-mail to test e-mail address ${testEmail}`);
    const emailSubject = `Test Subject - ${randomstring.generate()}`;
    await sendTestEmail(
      testEmail,
      emailSubject,
      "Hello, this is a test message."
    );

    let email;
    const startTime = Date.now();
    const timeout = 10000;
    const pollInterval = 100;

    while (!email && Date.now() - startTime < timeout) {
      try {
        email = await verifyEmailReceived(serverId, testEmail);
      } catch (error) {
        if (error.message !== "No email received") {
          throw error;
        }

        await new Promise((resolve) => setTimeout(resolve, pollInterval));
      }
    }

    if (!email) {
      throw new Error("Email was not received within the timeout period");
    }

    expect(email.subject).toBe(emailSubject);
  });
});
