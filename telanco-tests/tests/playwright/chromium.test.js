import { chromium } from "playwright";
import { test, expect } from "@playwright/test";
import MailosaurClient from "mailosaur";
import { verifyEmailReceived } from "../../send-email";

test.describe("WordPress Browsing Test Suite", () => {
  let browser;
  let page;

  test.beforeAll(async () => {
    browser = await chromium.launch();
    const context = await browser.newContext();
    page = await context.newPage();
  });

  test("should click read more, scroll down and go to new page", async () => {
    await page.goto("https://hop.steffen.codes/");
    const title = await page.title();
    const expectedHomepageTitle =
      "Webbureau med over 30 års erfaring - Telanco";

    expect(title).toBe(expectedHomepageTitle);

    await page.locator("#brxe-rtocjm").click();
    await expect(page.locator("#brxe-zivzii")).toBeVisible();

    await Promise.all([
      page.waitForNavigation(), // Wait for the navigation to happen
      page.locator("#brxe-zivzii").click() // Click the button
    ]);

    const newUrl = page.url();
    console.log("New URL:", newUrl);
    expect(newUrl).toBe("https://hop.steffen.codes/hjemmeside-eller-webshop/");
  });

  test("should hover over interactive svg and compare screenshot", async () => {
    await page.goto("https://hop.steffen.codes/hjemmeside-eller-webshop/");
    const interactivePhone = await page.$("#interactive-phone");
    await interactivePhone.scrollIntoViewIfNeeded();

    const securityBox = await page.$("[animate=security]");
    await securityBox.hover();
    await new Promise((resolve) => setTimeout(resolve, 2000));

    const phoneScreenshot = await interactivePhone.screenshot();
    expect(phoneScreenshot).toMatchSnapshot("interactive-phone.png");
  });

  test("should send email through contact us and receive confirmation email", async () => {
    await page.goto("https://hop.steffen.codes/kontakt-os/");
    const nameField = await page.$("[name=name]");
    const emailField = await page.$("[type=email]");
    const phoneField = await page.$("[name=phone]");
    const messageField = await page.$("[name=message]");
    const submitButton = await page.$("[type=submit]");

    const mailosaur = new MailosaurClient(process.env.MAILOSAUR_API_KEY);
    const serverId = process.env.MAILOSAUR_SERVER_ID;
    const testEmail = mailosaur.servers.generateEmailAddress(serverId);

    await nameField.type("My Name");
    await emailField.type(testEmail);
    await phoneField.type("12345678");
    await messageField.type("Hello World!");
    await submitButton.click();

    await page.waitForSelector(".message.success");

    let email;
    const startTime = Date.now();
    const timeout = 10000;
    const pollInterval = 100;

    while (!email && Date.now() - startTime < timeout) {
      try {
        email = await verifyEmailReceived(serverId, testEmail);
      } catch (error) {
        if (error.message !== "No email received") {
          throw error;
        }

        await new Promise((resolve) => setTimeout(resolve, pollInterval));
      }
    }

    if (!email) {
      throw new Error("Email was not received within the timeout period");
    }

    expect(email.subject).toBe("Tak for din henvendelse - Telanco");
  });
});
