
const section = document.getElementById('interactive-phone-section');
const container = document.getElementById('interactive-phone');

const elements = {
    sslBox: container.querySelector('#SSLBox'),
    sslEllipse: container.querySelector('#SSLEllipse'),
    illustration: container.querySelector('#illustration'),
    screenCover: container.querySelector('#screenCover'),
    codeCover: container.querySelector('#codeCover'),
    scrollToTop: container.querySelector('#phoneScrollToTop'),
    scrollToTopEllipse: container.querySelector('#phoneScrollToTopEllipse'),
    text: container.querySelector('#phoneText'),
    header: container.querySelector('#phoneHeader'),
    body: container.querySelector('#phoneBody'),
    ranksText: container.querySelector('#ranksText'),
    keywordsText: container.querySelector('#keywordsText'),
    support: container.querySelector('#support')
};

const handleOver = (animate) => {
    const actions = {
    content: () => { elements.body.style.opacity = 0.25; },
    seo: () => {
        elements.illustration.style.opacity = 0.25;
        elements.header.style.opacity = 0.25;
        elements.ranksText.style.fill = 'green';
        elements.keywordsText.style.fill = 'green';
    },
    code: () => {
        elements.screenCover.style.opacity = 0.5;
        elements.codeCover.style.opacity = 0;
    },
    functions: () => {
        elements.illustration.style.opacity = 0.25;
        elements.text.style.opacity = 0.25;
        elements.scrollToTop.style.opacity = 1;
        elements.scrollToTopEllipse.style.opacity = 1;
    },
    security: () => {
        elements.illustration.style.opacity = 0.25;
        elements.text.style.opacity = 0.25;
        elements.sslBox.style.opacity = 1;
        elements.sslEllipse.style.opacity = 0.3;
    },
    support: () => { elements.support.style.opacity = 1; }
    };
    if (actions[animate]) actions[animate]();
};

const handleOut = (animate) => {
    const resetActions = {
    content: () => { elements.body.style.opacity = ''; },
    seo: () => {
        elements.illustration.style.opacity = '';
        elements.header.style.opacity = '';
        elements.ranksText.style.fill = '';
        elements.keywordsText.style.fill = '';
    },
    code: () => {
        elements.screenCover.style.opacity = '';
        elements.codeCover.style.opacity = '';
    },
    functions: () => {
        elements.illustration.style.opacity = '';
        elements.text.style.opacity = '';
        elements.scrollToTop.style.opacity = '';
        elements.scrollToTopEllipse.style.opacity = '';
    },
    security: () => {
        elements.illustration.style.opacity = '';
        elements.text.style.opacity = '';
        elements.sslBox.style.opacity = '';
        elements.sslEllipse.style.opacity = '';
    },
    support: () => { elements.support.style.opacity = ''; }
    };
    if (resetActions[animate]) resetActions[animate]();
};

const boxes = container.querySelectorAll('.rounded');

boxes.forEach(box => {
    box.addEventListener('mouseover', () => {
    box.classList.add(section.classList.contains('bg-odd') ? 'hovered-odd' : 'hovered');
    handleOver(box.getAttribute('animate'));
    });

    box.addEventListener('mouseout', () => {
    box.classList.remove(section.classList.contains('bg-odd') ? 'hovered-odd' : 'hovered');
    handleOut(box.getAttribute('animate'));
    });
});